using ECSLiteTestTask.Links;

namespace ECSLiteTestTask.Views
{
    public interface IEntityView
    {
        public int Entity { get; set; }

        public IMonoComponent[] GetMonoComponents();
    }
}
