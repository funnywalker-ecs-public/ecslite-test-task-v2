using UnityEngine;

namespace ECSLiteTestTask.Views
{
    public class EntityView : MonoBehaviour, IEntityView
    {
        public int Entity { get; set; } = -1;

        public IMonoComponent[] GetMonoComponents() => GetComponentsInChildren<IMonoComponent>();
    }
}
