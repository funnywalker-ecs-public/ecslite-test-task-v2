namespace ECSLiteTestTask.Services
{
    public interface ITimeService
    {
        public float GetDeltaTime();
    }
}
