using UnityEngine;

namespace ECSLiteTestTask.Services
{
    public class UnityTimeService : ITimeService
    {
        public float GetDeltaTime() => Time.deltaTime;
    }
}
