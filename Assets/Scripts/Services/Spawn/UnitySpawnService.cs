using UnityEngine;

namespace ECSLiteTestTask.Services
{
    public class UnitySpawnService : ISpawnService
    {
        public void Spawn(object original, Vector3 position, Quaternion rotation)
        {
            Object.Instantiate(original as Object, position, rotation);
        }
    }
}
