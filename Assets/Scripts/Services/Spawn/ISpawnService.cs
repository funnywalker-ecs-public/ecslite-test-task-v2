using UnityEngine;

namespace ECSLiteTestTask.Services
{
    public interface ISpawnService
    {
        public void Spawn(object original, Vector3 position, Quaternion rotation);
    }
}
