using System.Linq;
using UnityEngine;

namespace ECSLiteTestTask.Services
{
    public class UnityObjectFinderService : IObjectFinderService
    {
        public T[] FindObjectsOfType<T>() => Object.FindObjectsOfType<MonoBehaviour>().OfType<T>().ToArray();
    }
}
