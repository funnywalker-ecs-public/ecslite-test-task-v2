namespace ECSLiteTestTask.Services
{
    public interface IObjectFinderService
    {
        public T[] FindObjectsOfType<T>();
    }
}
