using UnityEngine;
using UnityEngine.AI;

namespace ECSLiteTestTask.Services
{
    public class UnityNavigationService : INavigationService
    {
        public bool CalculateAgentPath(Vector3 sourcePosition, Vector3 targetPosition)
        {
            var path = new NavMeshPath();
            return NavMesh.CalculatePath(sourcePosition, targetPosition, NavMesh.AllAreas, path);
        }
    }
}
