using UnityEngine;

namespace ECSLiteTestTask.Services
{
    public interface INavigationService
    {
        public bool CalculateAgentPath(Vector3 sourcePosition, Vector3 targetPosition);
    }
}
