using UnityEngine;

namespace ECSLiteTestTask.Services
{
    public class UnityPhysicsService : IPhysicsService
    {
        public bool RaycastFromScreenPosition(Vector3 position, out RaycastHit hit, float distance, int layerMask)
        {
            var ray = Camera.main.ScreenPointToRay(position);
            if (Physics.Raycast(ray, out var raycastHit, distance, layerMask))
            {
                hit = raycastHit;
                return true;
            }

            hit = default;
            return false;
        }
    }
}
