using UnityEngine;

namespace ECSLiteTestTask.Services
{
    public interface IPhysicsService
    {
        public bool RaycastFromScreenPosition(Vector3 position, out RaycastHit hit, float distance, int layerMask);
    }
}
