using UnityEngine.SceneManagement;

namespace ECSLiteTestTask.Services
{
    public class UnityScenesService : IScenesService
    {
        public void LoadScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }

        public void ReloadScene()
        {
            LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
