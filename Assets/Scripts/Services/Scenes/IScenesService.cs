namespace ECSLiteTestTask.Services
{
    public interface IScenesService
    {
        public void LoadScene(string sceneName);
        public void ReloadScene();
    }
}
