using UnityEngine;

namespace ECSLiteTestTask.Services
{
    public interface IInputService
    {
        public bool GetMouseButtonUp(int index);
        public Vector2 GetMousePosition();
    }
}
