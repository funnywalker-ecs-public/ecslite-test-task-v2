using UnityEngine;

namespace ECSLiteTestTask.Services
{
    public class UnityInputService : IInputService
    {
        public bool GetMouseButtonUp(int index) => Input.GetMouseButtonUp(index);

        public Vector2 GetMousePosition() => Input.mousePosition;
    }
}
