using ECSLiteTestTask.Listeners;
using ECSLiteTestTask.Providers;
using UnityEngine;

namespace ECSLiteTestTask.Components
{
    #region Base Components
    
    public struct PositionComponent
    {
        public Vector3 Value;
    }

    public struct VelocityComponent
    {
        public Vector3 Value;
    }

    public struct MaxSpeedComponent
    {
        public float Value;
    }
    
    public struct ColorComponent
    {
        public Color Value;
    }

    public struct InstanceComponent
    {
        public int Value;
    }

    public struct VisibleComponent
    {
        public bool Value;
    }

    #endregion
    
    public struct PlayerComponent { }
    
    public struct ButtonComponent { }
    
    public struct PressedButtonComponent { }
    
    public struct DoorComponent { }

    public struct OpenedDoorComponent { }
    
    public struct InteractableComponent { }
    
    public struct CameraTargetComponent { }

    public struct DoorRelatedButtonComponent
    {
        public int Button;
    }
    
    public struct FinishComponent { }
    
    public struct CameraRootComponent { }
    
    #region Input Events

    public struct MouseInputEvent
    {
        public Vector2 Position;
    }

    #endregion

    #region Navigation

    public struct NavigationAgentComponent { }
    
    public struct NavigationTargetComponent { }
    
    public struct HasNavigationTarget { }

    #endregion
    
    #region Navigation Events

    public struct UpdateNavigationAgentTargetEvent { }
    
    #endregion
    
    #region Events

    public struct ButtonPressedEvent
    {
        public int Button;
    }
    
    public struct ButtonUnpressedEvent
    {
        public int Button;
    }
    
    public struct FinishLevelEvent { }

    #endregion
    
    #region Listeners

    public struct PositionListenerComponent
    {
        public IPositionListener Listener;
    }

    public struct VisibleListenerComponent
    {
        public IVisibleListener Listener;
    }

    public struct NavigationListenerComponent
    {
        public INavigationListener Listener;
    }

    public struct ColorListenerComponent
    {
        public IColorListener Listener;
    }

    public struct AnimatorBoolParameterListenerComponent
    {
        public IAnimatorBoolParameterListener Listener;
    }
    
    public struct AnimatorFloatParameterListenerComponent
    {
        public IAnimatorFloatParameterListener Listener;
    }
    
    #endregion

    #region Providers

    public struct NavigationVelocityProviderComponent
    {
        public IVelocityProvider Provider;
    }

    #endregion
}
