using ECSLiteTestTask.Components;
using UnityEngine;
using UnityEngine.AI;

namespace ECSLiteTestTask.Providers
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class NavigationVelocityProvider : Provider<NavigationVelocityProviderComponent>, IVelocityProvider
    {
        private NavMeshAgent _navMeshAgent;

        private void Awake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        public override void SetProvider(ref NavigationVelocityProviderComponent component) => component.Provider = this;

        public Vector3 GetVelocity() => _navMeshAgent.velocity;
    }
}
