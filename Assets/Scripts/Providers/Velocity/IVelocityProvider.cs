using UnityEngine;

namespace ECSLiteTestTask.Providers
{
    public interface IVelocityProvider
    {
        public Vector3 GetVelocity();
    }
}
