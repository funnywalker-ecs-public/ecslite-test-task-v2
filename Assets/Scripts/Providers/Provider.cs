using ECSLiteTestTask.Views;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECSLiteTestTask.Providers
{
    public abstract class Provider<T> : MonoBehaviour, IMonoComponent where T : struct
    {
        public void Register(int entity, EcsWorld ecsWorld)
        {
            ref var component = ref ecsWorld.GetPool<T>().Add(entity);
            SetProvider(ref component);
        }
        
        public abstract void SetProvider(ref T component);
    }
}
