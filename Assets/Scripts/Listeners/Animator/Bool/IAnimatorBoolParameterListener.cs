namespace ECSLiteTestTask.Listeners
{
    public interface IAnimatorBoolParameterListener
    {
        public void OnAnimatorParameterUpdate(string parameterName, bool value);
    }
}
