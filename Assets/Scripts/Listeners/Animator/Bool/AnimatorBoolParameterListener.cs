using ECSLiteTestTask.Components;
using UnityEngine;

namespace ECSLiteTestTask.Listeners
{
    [RequireComponent(typeof(Animator))]
    public class AnimatorBoolParameterListener : Listener<AnimatorBoolParameterListenerComponent>, IAnimatorBoolParameterListener
    {
        private Animator _animator;
        
        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        public override void SetListener(ref AnimatorBoolParameterListenerComponent component) => component.Listener = this;

        public void OnAnimatorParameterUpdate(string parameterName, bool value)
        {
            if (_animator.GetBool(parameterName) != value)
            {
                _animator.SetBool(parameterName, value);
            }
        }
    }
}
