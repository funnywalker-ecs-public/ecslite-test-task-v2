namespace ECSLiteTestTask.Listeners
{
    public interface IAnimatorFloatParameterListener
    {
        public void OnAnimatorParameterUpdate(string parameterName, float value);
    }
}
