using System;
using ECSLiteTestTask.Components;
using UnityEngine;

namespace ECSLiteTestTask.Listeners
{
    [RequireComponent(typeof(Animator))]
    public class AnimatorFloatParameterListener : Listener<AnimatorFloatParameterListenerComponent>, IAnimatorFloatParameterListener
    {
        private Animator _animator;
        
        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        public override void SetListener(ref AnimatorFloatParameterListenerComponent component) => component.Listener = this;

        public void OnAnimatorParameterUpdate(string parameterName, float value)
        {
            if (Math.Abs(_animator.GetFloat(parameterName) - value) > 0.001f)
            {
                _animator.SetFloat(parameterName, value);
            }
        }
    }
}