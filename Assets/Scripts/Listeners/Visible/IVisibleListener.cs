namespace ECSLiteTestTask.Listeners
{
    public interface IVisibleListener
    {
        public void OnVisibleUpdate(bool value);
    }
}
