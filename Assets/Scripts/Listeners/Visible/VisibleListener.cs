using ECSLiteTestTask.Components;

namespace ECSLiteTestTask.Listeners
{
    public class VisibleListener : Listener<VisibleListenerComponent>, IVisibleListener
    {
        public override void SetListener(ref VisibleListenerComponent component) => component.Listener = this;
        
        public void OnVisibleUpdate(bool value)
        {
            if (gameObject.activeSelf != value)
            {
                gameObject.SetActive(value);
            }
        }
    }
}