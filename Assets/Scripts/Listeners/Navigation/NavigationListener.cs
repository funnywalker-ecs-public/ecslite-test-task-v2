using ECSLiteTestTask.Components;
using UnityEngine;
using UnityEngine.AI;

namespace ECSLiteTestTask.Listeners
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class NavigationListener : Listener<NavigationListenerComponent>, INavigationListener
    {
        private NavMeshAgent _navMeshAgent;
        
        private void Awake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        public override void SetListener(ref NavigationListenerComponent component) => component.Listener = this;
        
        public void OnNavigationTargetUpdate(Vector3 targetPosition)
        {
            if (_navMeshAgent.pathEndPosition != targetPosition)
            {
                _navMeshAgent.SetDestination(targetPosition);
            }
        }
    }
}
