using UnityEngine;

namespace ECSLiteTestTask.Listeners
{
    public interface INavigationListener
    {
        public void OnNavigationTargetUpdate(Vector3 targetPosition);
    }
}
