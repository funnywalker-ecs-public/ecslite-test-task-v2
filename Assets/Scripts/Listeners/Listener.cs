using ECSLiteTestTask.Views;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECSLiteTestTask.Listeners
{
    public abstract class Listener<T> : MonoBehaviour, IMonoComponent where T : struct
    {
        public void Register(int entity, EcsWorld ecsWorld)
        {
            ref var component = ref ecsWorld.GetPool<T>().Add(entity);
            SetListener(ref component);
        }

        public abstract void SetListener(ref T component);
    }
}
