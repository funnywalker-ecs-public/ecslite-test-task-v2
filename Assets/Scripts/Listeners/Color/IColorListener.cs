using UnityEngine;

namespace ECSLiteTestTask.Listeners
{
    public interface IColorListener
    {
        public void OnColorUpdate(Color value);
    }
}
