using ECSLiteTestTask.Components;
using UnityEngine;

namespace ECSLiteTestTask.Listeners
{
    public class MeshRendererColorListener : Listener<ColorListenerComponent>, IColorListener
    {
        [SerializeField] private MeshRenderer _meshRenderer;

        public override void SetListener(ref ColorListenerComponent component) => component.Listener = this;

        public void OnColorUpdate(Color value)
        {
            if (_meshRenderer.material.color != value)
            {
                _meshRenderer.material.color = value;
            }
        }
    }
}
