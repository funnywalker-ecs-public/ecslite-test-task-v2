using UnityEngine;

namespace ECSLiteTestTask.Listeners
{
    public interface IPositionListener
    {
        public void OnPositionUpdate(Vector3 value);
    }
}
