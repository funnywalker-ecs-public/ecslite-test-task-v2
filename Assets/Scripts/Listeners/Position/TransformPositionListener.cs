using ECSLiteTestTask.Components;
using UnityEngine;

namespace ECSLiteTestTask.Listeners
{
    public class TransformPositionListener : Listener<PositionListenerComponent>, IPositionListener
    {
        public override void SetListener(ref PositionListenerComponent listenerComponent) => listenerComponent.Listener = this;
        
        public void OnPositionUpdate(Vector3 value)
        {
            if (transform.position != value)
            {
                transform.position = value;
            }
        }
    }
}
