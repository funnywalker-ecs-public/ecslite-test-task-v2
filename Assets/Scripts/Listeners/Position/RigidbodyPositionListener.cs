using ECSLiteTestTask.Components;
using UnityEngine;

namespace ECSLiteTestTask.Listeners
{
    public class RigidbodyPositionListener : Listener<PositionListenerComponent>, IPositionListener
    {
        private Rigidbody _rigidbody;
        
        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        public override void SetListener(ref PositionListenerComponent listenerComponent) => listenerComponent.Listener = this;
        
        public void OnPositionUpdate(Vector3 value)
        {
            if (transform.position != value)
            {
                _rigidbody.position = value;
            }
        }
    }
}