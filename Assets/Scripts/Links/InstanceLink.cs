using ECSLiteTestTask.Components;

namespace ECSLiteTestTask.Links
{
    public class InstanceLink : MonoComponentLink<InstanceComponent>
    {
        public override void SetComponentValues(ref InstanceComponent component)
        {
            component.Value = gameObject.GetInstanceID();
        }
    }
}
