using ECSLiteTestTask.Components;

namespace ECSLiteTestTask.Links
{
    public class MaxSpeedLink : MonoComponentLink<MaxSpeedComponent>
    {
        public override void SetComponentValues(ref MaxSpeedComponent component)
        {
            component.Value = 3.5f;
        }
    }
}
