using ECSLiteTestTask.Views;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECSLiteTestTask.Links
{
    public class MonoComponentLink<T> : MonoBehaviour, IMonoComponent where T : struct
    {
        public void Register(int entity, EcsWorld ecsWorld)
        {
            ref var component = ref ecsWorld.GetPool<T>().Add(entity);
            SetComponentValues(ref component);
            Destroy(this);
        }

        public virtual void SetComponentValues(ref T component)
        {
        }
    }
}
