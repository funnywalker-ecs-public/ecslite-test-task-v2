using ECSLiteTestTask.Components;

namespace ECSLiteTestTask.Links
{
    public class VisibleLink : MonoComponentLink<VisibleComponent>
    {
        public override void SetComponentValues(ref VisibleComponent component)
        {
            component.Value = gameObject.activeSelf;
        }
    }
}
