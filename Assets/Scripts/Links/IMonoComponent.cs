using Leopotam.EcsLite;

namespace ECSLiteTestTask.Views
{
    public interface IMonoComponent
    {
        public void Register(int entity, EcsWorld ecsWorld);
    }
}
