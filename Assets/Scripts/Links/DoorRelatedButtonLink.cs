using ECSLiteTestTask.Components;
using UnityEditor;
using UnityEngine;

namespace ECSLiteTestTask.Links
{
    public class DoorRelatedButtonLink : MonoComponentLink<DoorRelatedButtonComponent>
    {
        [SerializeField] private GameObject _button;

        public override void SetComponentValues(ref DoorRelatedButtonComponent component)
        {
            component.Button = _button.GetInstanceID();
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (_button != null)
            {
                var point1 = transform.position;
                var point2 = _button.transform.position;
                Handles.DrawBezier(point1, point2, point1, point2, Color.green, null, 5);
            }
        }
#endif
    }
}
