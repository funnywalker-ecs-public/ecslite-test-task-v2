using ECSLiteTestTask.Components;
using UnityEngine;

namespace ECSLiteTestTask.Links
{
    public class ColorLink : MonoComponentLink<ColorComponent>
    {
        [SerializeField] private Color _color = Color.white;

        public override void SetComponentValues(ref ColorComponent component)
        {
            component.Value = _color;
        }
    }
}
