using ECSLiteTestTask.Components;

namespace ECSLiteTestTask.Links
{
    public class PositionLink : MonoComponentLink<PositionComponent>
    {
        public override void SetComponentValues(ref PositionComponent component)
        {
            component.Value = transform.position;
        }
    }
}
