using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class UpdateAgentAnimationParameterMoveSpeedSystem : IEcsRunSystem
    {
        private readonly EcsFilterInject<Inc<NavigationAgentComponent, HasNavigationTarget, VelocityComponent, MaxSpeedComponent, AnimatorFloatParameterListenerComponent>> _agentFilter = default;
        
        private readonly EcsPoolInject<VelocityComponent> _velocityPool = default;
        private readonly EcsPoolInject<MaxSpeedComponent> _maxSpeedPool = default;
        private readonly EcsPoolInject<AnimatorFloatParameterListenerComponent> _animatorFloatParameterListenerPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var agentEntity in _agentFilter.Value)
            {
                ref var agentVelocityComponent = ref _velocityPool.Value.Get(agentEntity);
                ref var agentMaxSpeedComponent = ref _maxSpeedPool.Value.Get(agentEntity);
                ref var animatorFloatParameterListenerComponent = ref _animatorFloatParameterListenerPool.Value.Get(agentEntity);
                
                var currentSpeed = agentVelocityComponent.Value.magnitude;
                var maxSpeed = agentMaxSpeedComponent.Value;
                var normalizedSpeed = currentSpeed / maxSpeed;
                
                animatorFloatParameterListenerComponent.Listener.OnAnimatorParameterUpdate(GameConstants.ANIMATOR_FLOAT_MOVESPEED, normalizedSpeed);
            }
        }
    }
}
