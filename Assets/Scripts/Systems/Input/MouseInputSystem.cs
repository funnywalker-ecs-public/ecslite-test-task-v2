using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using ECSLiteTestTask.Services;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class MouseInputSystem : IEcsRunSystem
    {
        private readonly EcsCustomInject<IInputService> _inputService = default;

        private readonly EcsWorldInject _ecsWorldEvents = GameConstants.EVENTS_WORLD;
        
        private readonly EcsPoolInject<MouseInputEvent> _mouseInputPool = GameConstants.EVENTS_WORLD;

        public void Run(EcsSystems systems)
        {
            if (_inputService.Value.GetMouseButtonUp(0))
            {
                ref var mouseInputEvent = ref _mouseInputPool.Value.Add(_ecsWorldEvents.Value.NewEntity());
                mouseInputEvent.Position = _inputService.Value.GetMousePosition();
            }
        }
    }
}
