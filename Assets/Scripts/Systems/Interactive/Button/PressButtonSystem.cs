using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class PressButtonSystem : IEcsRunSystem
    {
        private readonly EcsFilterInject<Inc<ButtonPressedEvent>> _buttonPressedFilter = GameConstants.EVENTS_WORLD;
        
        private readonly EcsFilterInject<Inc<ButtonComponent, InstanceComponent, InteractableComponent, AnimatorBoolParameterListenerComponent>, Exc<PressedButtonComponent>> _interactableButtonFilter = default;
        
        private readonly EcsPoolInject<ButtonPressedEvent> _buttonPressedPool = GameConstants.EVENTS_WORLD;
        
        private readonly EcsPoolInject<InstanceComponent> _instancePool = default;
        private readonly EcsPoolInject<PressedButtonComponent> _pressedButtonPool = default;
        private readonly EcsPoolInject<AnimatorBoolParameterListenerComponent> _animatorBoolParameterListenerPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var buttonPressedEntity in _buttonPressedFilter.Value)
            {
                ref var buttonPressedEvent = ref _buttonPressedPool.Value.Get(buttonPressedEntity);
                
                foreach (var interactableButtonEntity in _interactableButtonFilter.Value)
                {
                    ref var buttonInstanceComponent = ref _instancePool.Value.Get(interactableButtonEntity);
                    
                    if (buttonPressedEvent.Button == buttonInstanceComponent.Value)
                    {
                        _pressedButtonPool.Value.Add(interactableButtonEntity);
                        
                        ref var animatorBoolParameterListenerComponent = ref _animatorBoolParameterListenerPool.Value.Get(interactableButtonEntity);
                        animatorBoolParameterListenerComponent.Listener.OnAnimatorParameterUpdate(GameConstants.ANIMATOR_BOOL_IDLE, false);
                        animatorBoolParameterListenerComponent.Listener.OnAnimatorParameterUpdate(GameConstants.ANIMATOR_BOOL_PRESS, true);
                    }
                }
            }
        }
    }
}
