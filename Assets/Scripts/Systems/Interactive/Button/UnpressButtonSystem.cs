using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class UnpressButtonSystem : IEcsRunSystem
    {
        private readonly EcsFilterInject<Inc<ButtonUnpressedEvent>> _buttonUnpressedFilter = GameConstants.EVENTS_WORLD;
        
        private readonly EcsFilterInject<Inc<ButtonComponent, InstanceComponent, InteractableComponent, PressedButtonComponent, AnimatorBoolParameterListenerComponent>> _interactableButtonFilter = default;
        
        private readonly EcsPoolInject<ButtonUnpressedEvent> _buttonUnpressedPool = GameConstants.EVENTS_WORLD;
        
        private readonly EcsPoolInject<InstanceComponent> _instancePool = default;
        private readonly EcsPoolInject<PressedButtonComponent> _pressedButtonPool = default;
        private readonly EcsPoolInject<AnimatorBoolParameterListenerComponent> _animatorBoolParameterListenerPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var buttonUnpressedEntity in _buttonUnpressedFilter.Value)
            {
                ref var buttonUnpressedEvent = ref _buttonUnpressedPool.Value.Get(buttonUnpressedEntity);
                
                foreach (var interactableButtonEntity in _interactableButtonFilter.Value)
                {
                    ref var buttonInstanceComponent = ref _instancePool.Value.Get(interactableButtonEntity);
                    
                    if (buttonUnpressedEvent.Button == buttonInstanceComponent.Value)
                    {
                        _pressedButtonPool.Value.Del(interactableButtonEntity);
                
                        ref var animatorBoolParameterListenerComponent = ref _animatorBoolParameterListenerPool.Value.Get(interactableButtonEntity);
                        animatorBoolParameterListenerComponent.Listener.OnAnimatorParameterUpdate(GameConstants.ANIMATOR_BOOL_PRESS, false);
                        animatorBoolParameterListenerComponent.Listener.OnAnimatorParameterUpdate(GameConstants.ANIMATOR_BOOL_IDLE, true);
                    }
                }
            }
        }
    }
}
