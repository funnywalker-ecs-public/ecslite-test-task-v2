using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using LeoEcsPhysics;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class ButtonTriggerExitSystem : IEcsRunSystem
    {
        private readonly EcsWorldInject _ecsWorldEvents = GameConstants.EVENTS_WORLD;
        
        private readonly EcsFilterInject<Inc<OnTriggerExitEvent>> _triggerExitFilter = GameConstants.PHYSICS_WORLD;

        private readonly EcsFilterInject<Inc<ButtonComponent, InstanceComponent, InteractableComponent, PressedButtonComponent>> _interactableButtonFilter = default;
        
        private readonly EcsPoolInject<ButtonUnpressedEvent> _buttonUnpressedPool = GameConstants.EVENTS_WORLD;

        private readonly EcsPoolInject<OnTriggerExitEvent> _triggerExitPool = GameConstants.PHYSICS_WORLD;

        private readonly EcsPoolInject<InstanceComponent> _instancePool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _triggerExitFilter.Value)
            {
                ref var triggerExitEvent = ref _triggerExitPool.Value.Get(entity);

                foreach (var interactableButtonEntity in _interactableButtonFilter.Value)
                {
                    ref var instanceComponent = ref _instancePool.Value.Get(interactableButtonEntity);
                    
                    if (instanceComponent.Value == triggerExitEvent.collider.gameObject.GetInstanceID())
                    {
                        ref var buttonUnpressedEvent = ref _buttonUnpressedPool.Value.Add(_ecsWorldEvents.Value.NewEntity());
                        buttonUnpressedEvent.Button = instanceComponent.Value;
                    }
                }
            }
        }
    }
}