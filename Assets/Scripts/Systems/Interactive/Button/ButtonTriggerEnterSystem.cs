using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using LeoEcsPhysics;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class ButtonTriggerEnterSystem : IEcsRunSystem
    {
        private readonly EcsWorldInject _ecsWorldEvents = GameConstants.EVENTS_WORLD;
        
        private readonly EcsFilterInject<Inc<OnTriggerEnterEvent>> _triggerEnterFilter = GameConstants.PHYSICS_WORLD;

        private readonly EcsFilterInject<Inc<ButtonComponent, InstanceComponent, InteractableComponent>, Exc<PressedButtonComponent>> _interactableButtonFilter = default;
        
        private readonly EcsPoolInject<ButtonPressedEvent> _buttonPressedPool = GameConstants.EVENTS_WORLD;

        private readonly EcsPoolInject<OnTriggerEnterEvent> _triggerEnterPool = GameConstants.PHYSICS_WORLD;

        private readonly EcsPoolInject<InstanceComponent> _instancePool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _triggerEnterFilter.Value)
            {
                ref var triggerEnterEvent = ref _triggerEnterPool.Value.Get(entity);

                foreach (var interactableButtonEntity in _interactableButtonFilter.Value)
                {
                    ref var instanceComponent = ref _instancePool.Value.Get(interactableButtonEntity);
                    
                    if (instanceComponent.Value == triggerEnterEvent.collider.gameObject.GetInstanceID())
                    {
                        ref var buttonPressedEvent = ref _buttonPressedPool.Value.Add(_ecsWorldEvents.Value.NewEntity());
                        buttonPressedEvent.Button = instanceComponent.Value;
                    }
                }
            }
        }
    }
}
