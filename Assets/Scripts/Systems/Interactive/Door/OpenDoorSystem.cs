using ECSLiteTestTask.Components;
using ECSLiteTestTask.Configs;
using ECSLiteTestTask.Services;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

namespace ECSLiteTestTask.Systems
{
    public class OpenDoorSystem : IEcsRunSystem
    {
        private readonly EcsCustomInject<GameConfig> _gameConfig = default;
        private readonly EcsCustomInject<ITimeService> _timeService = default;

        private readonly EcsFilterInject<Inc<DoorComponent, DoorRelatedButtonComponent, PositionComponent, PositionListenerComponent>, Exc<OpenedDoorComponent>> _doorFilter = default;
        private readonly EcsFilterInject<Inc<ButtonComponent, InteractableComponent, InstanceComponent, PressedButtonComponent>> _buttonFilter = default;

        private readonly EcsPoolInject<DoorRelatedButtonComponent> _doorRelatedButtonPool = default;
        private readonly EcsPoolInject<OpenedDoorComponent> _openedDoorPool = default;
        private readonly EcsPoolInject<InstanceComponent> _instancePool = default;
        private readonly EcsPoolInject<PositionComponent> _positionPool = default;
        private readonly EcsPoolInject<PositionListenerComponent> _positionListenerPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var doorEntity in _doorFilter.Value)
            {
                ref var doorRelatedButtonComponent = ref _doorRelatedButtonPool.Value.Get(doorEntity);
                ref var doorPositionComponent = ref _positionPool.Value.Get(doorEntity);
                ref var doorPositionListenerComponent = ref _positionListenerPool.Value.Get(doorEntity);

                foreach (var buttonEntity in _buttonFilter.Value)
                {
                    ref var buttonInstance = ref _instancePool.Value.Get(buttonEntity);

                    if (doorRelatedButtonComponent.Button == buttonInstance.Value)
                    {
                        doorPositionComponent.Value += Vector3.down * _gameConfig.Value.DoorOpenSpeed * _timeService.Value.GetDeltaTime();
                        doorPositionListenerComponent.Listener.OnPositionUpdate(doorPositionComponent.Value);

                        if (doorPositionComponent.Value.y <= _gameConfig.Value.DoorOpenHeightThreshold)
                        {
                            _openedDoorPool.Value.Add(doorEntity);
                        }
                    }
                }
            }
        }
    }
}
