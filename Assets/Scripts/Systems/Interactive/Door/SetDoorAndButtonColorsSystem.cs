using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class SetDoorAndButtonColorsSystem : IEcsInitSystem
    {
        private readonly EcsFilterInject<Inc<DoorComponent, DoorRelatedButtonComponent, ColorComponent, ColorListenerComponent>> _doorFilter = default;
        private readonly EcsFilterInject<Inc<ButtonComponent, InstanceComponent, ColorListenerComponent>> _buttonFilter = default;

        private readonly EcsPoolInject<DoorRelatedButtonComponent> _doorRelatedButtonPool = default;
        private readonly EcsPoolInject<ColorComponent> _colorPool = default;
        private readonly EcsPoolInject<InstanceComponent> _instancePool = default;
        private readonly EcsPoolInject<ColorListenerComponent> _colorListenerPool = default;

        public void Init(EcsSystems systems)
        {
            foreach (var doorEntity in _doorFilter.Value)
            {
                ref var doorRelatedButtonComponent = ref _doorRelatedButtonPool.Value.Get(doorEntity);
                ref var doorColorComponent = ref _colorPool.Value.Get(doorEntity);
                ref var doorColorListenerComponent = ref _colorListenerPool.Value.Get(doorEntity);
                
                doorColorListenerComponent.Listener.OnColorUpdate(doorColorComponent.Value);

                foreach (var buttonEntity in _buttonFilter.Value)
                {
                    ref var buttonInstanceComponent = ref _instancePool.Value.Get(buttonEntity);

                    if (buttonInstanceComponent.Value == doorRelatedButtonComponent.Button)
                    {
                        ref var buttonColorComponent = ref _colorPool.Value.Add(buttonEntity);
                        buttonColorComponent.Value = doorColorComponent.Value;
                        
                        ref var buttonColorListenerComponent = ref _colorListenerPool.Value.Get(buttonEntity);
                        buttonColorListenerComponent.Listener.OnColorUpdate(buttonColorComponent.Value);
                    }
                }
            }
        }
    }
}
