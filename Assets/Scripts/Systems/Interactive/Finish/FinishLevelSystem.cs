using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using ECSLiteTestTask.Services;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class FinishLevelSystem : IEcsRunSystem
    {
        private readonly EcsCustomInject<IScenesService> _scenesService = default;

        private readonly EcsFilterInject<Inc<FinishLevelEvent>> _finishLevelFilter = GameConstants.EVENTS_WORLD;

        private readonly EcsPoolInject<FinishLevelEvent> _finishLevelPool = GameConstants.EVENTS_WORLD;

        public void Run(EcsSystems systems)
        {
            foreach (var finishLevelEntity in _finishLevelFilter.Value)
            {
                _scenesService.Value.ReloadScene();
            }
        }
    }
}
