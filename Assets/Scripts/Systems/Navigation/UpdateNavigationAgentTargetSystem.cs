using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using ECSLiteTestTask.Services;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class UpdateNavigationAgentTargetSystem : IEcsRunSystem
    {
        private readonly EcsCustomInject<INavigationService> _navigationService = default;

        private readonly EcsFilterInject<Inc<UpdateNavigationAgentTargetEvent>> _updateTargetFilter = GameConstants.EVENTS_WORLD;
        
        private readonly EcsFilterInject<Inc<NavigationTargetComponent, InstanceComponent, PositionComponent>> _targetFilter = default;
        private readonly EcsFilterInject<Inc<NavigationAgentComponent, PositionComponent, NavigationListenerComponent>> _agentFilter = default;

        private readonly EcsPoolInject<HasNavigationTarget> _hasTargetPool = default;
        private readonly EcsPoolInject<PositionComponent> _positionPool = default;
        private readonly EcsPoolInject<NavigationListenerComponent> _navigationListenerPool = default;
        private readonly EcsPoolInject<VisibleComponent> _visiblePool = default;
        private readonly EcsPoolInject<VisibleListenerComponent> _visibleListenerPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var updateTargetEntity in _updateTargetFilter.Value)
            {
                foreach (var targetEntity in _targetFilter.Value)
                {
                    ref var targetPositionComponent = ref _positionPool.Value.Get(targetEntity);

                    foreach (var agentEntity in _agentFilter.Value)
                    {
                        ref var agentPositionComponent = ref _positionPool.Value.Get(agentEntity);

                        if (_navigationService.Value.CalculateAgentPath(agentPositionComponent.Value, targetPositionComponent.Value))
                        {
                            ref var navigationListenerComponent = ref _navigationListenerPool.Value.Get(agentEntity);
                            navigationListenerComponent.Listener.OnNavigationTargetUpdate(targetPositionComponent.Value);
                            
                            if (!_hasTargetPool.Value.Has(agentEntity))
                            {
                                _hasTargetPool.Value.Add(agentEntity);
                                
                                ref var targetVisibleComponent = ref _visiblePool.Value.Get(targetEntity);
                                ref var targetVisibleListenerComponent = ref _visibleListenerPool.Value.Get(targetEntity);

                                targetVisibleComponent.Value = true;
                                targetVisibleListenerComponent.Listener.OnVisibleUpdate(targetVisibleComponent.Value);
                            }
                        }
                    }
                }
            }
        }
    }
}
