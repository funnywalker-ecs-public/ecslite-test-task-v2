using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class CalculateNavigationAgentVelocitySystem : IEcsRunSystem
    {
        private readonly EcsFilterInject<Inc<NavigationAgentComponent, VelocityComponent, NavigationVelocityProviderComponent>> _filter = default;

        private readonly EcsPoolInject<VelocityComponent> _velocityPool = default;
        private readonly EcsPoolInject<NavigationVelocityProviderComponent> _velocityProviderPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter.Value)
            {
                ref var velocityComponent = ref _velocityPool.Value.Get(entity);
                ref var velocityProviderComponent = ref _velocityProviderPool.Value.Get(entity);

                velocityComponent.Value = velocityProviderComponent.Provider.GetVelocity();
            }
        }
    }
}