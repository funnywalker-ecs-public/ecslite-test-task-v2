using ECSLiteTestTask.Common;
using ECSLiteTestTask.Components;
using ECSLiteTestTask.Services;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class UpdateNavigationTargetPositionSystem : IEcsRunSystem
    {
        private readonly EcsCustomInject<IPhysicsService> _physicsService = default;
        private readonly EcsCustomInject<SceneInfo> _sceneInfo = default;
        
        private readonly EcsWorldInject _ecsWorldEvents = GameConstants.EVENTS_WORLD;
        
        private readonly EcsFilterInject<Inc<MouseInputEvent>> _mouseInputFilter = GameConstants.EVENTS_WORLD;
        
        private readonly EcsFilterInject<Inc<NavigationTargetComponent, PositionComponent>> _targetFilter = default;

        private readonly EcsPoolInject<MouseInputEvent> _mouseInputPool = GameConstants.EVENTS_WORLD;
        private readonly EcsPoolInject<UpdateNavigationAgentTargetEvent> _updateTargetPool = GameConstants.EVENTS_WORLD;

        private readonly EcsPoolInject<PositionComponent> _positionPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var mouseInputEntity in _mouseInputFilter.Value)
            {
                ref var mouseInputEvent = ref _mouseInputPool.Value.Get(mouseInputEntity);

                if (_physicsService.Value.RaycastFromScreenPosition(mouseInputEvent.Position, out var hit, 100.0f, 1 << 10))
                {
                    foreach (var targetEntity in _targetFilter.Value)
                    {
                        ref var positionComponent = ref _positionPool.Value.Get(targetEntity);
                        positionComponent.Value = hit.point;
                    
                        _updateTargetPool.Value.Add(_ecsWorldEvents.Value.NewEntity());
                    }
                }
            }
        }
    }
}
