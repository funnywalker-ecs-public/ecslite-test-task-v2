using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

namespace ECSLiteTestTask.Systems
{
    public class ProcessNavigationAgentSystem : IEcsRunSystem
    {
        private readonly EcsFilterInject<Inc<NavigationAgentComponent, HasNavigationTarget, PositionComponent>> _agentFilter = default;
        private readonly EcsFilterInject<Inc<NavigationTargetComponent, PositionComponent, VisibleComponent, VisibleListenerComponent>> _targetFilter = default;
    
        private readonly EcsPoolInject<HasNavigationTarget> _hasTargetPool = default;
        private readonly EcsPoolInject<PositionComponent> _positionPool = default;
        private readonly EcsPoolInject<VisibleComponent> _visiblePool = default;
        private readonly EcsPoolInject<VisibleListenerComponent> _visibleListenerPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var agentEntity in _agentFilter.Value)
            {
                ref var agentPositionComponent = ref _positionPool.Value.Get(agentEntity);

                foreach (var targetEntity in _targetFilter.Value)
                {
                    ref var targetPositionComponent = ref _positionPool.Value.Get(targetEntity);
                        
                    if (Vector3.Distance(agentPositionComponent.Value, targetPositionComponent.Value) < 0.01f)
                    {
                        _hasTargetPool.Value.Del(agentEntity);
                            
                        ref var targetVisibleComponent = ref _visiblePool.Value.Get(targetEntity);
                        ref var targetVisibleListenerComponent = ref _visibleListenerPool.Value.Get(targetEntity);

                        targetVisibleComponent.Value = false;
                        targetVisibleListenerComponent.Listener.OnVisibleUpdate(targetVisibleComponent.Value);
                    }
                }
            }
        }
    }
}
