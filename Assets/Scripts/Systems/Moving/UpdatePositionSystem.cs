using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class UpdatePositionSystem : IEcsRunSystem
    {
        private readonly EcsFilterInject<Inc<PositionComponent, PositionListenerComponent>> _filter = default;

        private readonly EcsPoolInject<PositionComponent> _positionPool = default;
        private readonly EcsPoolInject<PositionListenerComponent> _positionListenerPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter.Value)
            {
                ref var positionComponent = ref _positionPool.Value.Get(entity);
                ref var positionListenerComponent = ref _positionListenerPool.Value.Get(entity);
                
                positionListenerComponent.Listener.OnPositionUpdate(positionComponent.Value);
            }
        }
    }
}
