using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

namespace ECSLiteTestTask.Systems
{
    public class CameraMoveSystem : IEcsRunSystem
    {
        private readonly EcsFilterInject<Inc<CameraRootComponent, VelocityComponent, PositionComponent>> _cameraRootFilter = default;
        private readonly EcsFilterInject<Inc<CameraTargetComponent, PositionComponent>> _targetFilter = default;

        private readonly EcsPoolInject<PositionComponent> _positionPool = default;
        private readonly EcsPoolInject<VelocityComponent> _velocityPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var cameraRootEntity in _cameraRootFilter.Value)
            {
                ref var cameraRootPositionComponent = ref _positionPool.Value.Get(cameraRootEntity);
                ref var cameraRootVelocityComponent = ref _velocityPool.Value.Get(cameraRootEntity);
                
                foreach (var targetEntity in _targetFilter.Value)
                {
                    ref var targetPositionComponent = ref _positionPool.Value.Get(targetEntity);
                    var currentPosition = cameraRootPositionComponent.Value;
                    var targetPosition = targetPositionComponent.Value;
                    
                    var newPosition = Vector3.SmoothDamp(currentPosition, targetPosition, ref cameraRootVelocityComponent.Value, 0.3f);
                    
                    cameraRootPositionComponent.Value = newPosition;
                }
            }
            
        }
    }
}
