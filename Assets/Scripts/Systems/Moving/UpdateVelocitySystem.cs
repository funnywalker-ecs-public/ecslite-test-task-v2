using ECSLiteTestTask.Components;
using ECSLiteTestTask.Services;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class UpdateVelocitySystem : IEcsRunSystem
    {
        private readonly EcsCustomInject<ITimeService> _timeService = default;

        private readonly EcsFilterInject<Inc<PositionComponent, VelocityComponent>> _filter = default;

        private readonly EcsPoolInject<PositionComponent> _positionPool = default;
        private readonly EcsPoolInject<VelocityComponent> _velocityPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter.Value)
            {
                ref var positionComponent = ref _positionPool.Value.Get(entity);
                ref var velocityComponent = ref _velocityPool.Value.Get(entity);

                positionComponent.Value += velocityComponent.Value * _timeService.Value.GetDeltaTime();
            }
        }
    }
}