using ECSLiteTestTask.Components;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class UpdateVisibleSystem : IEcsRunSystem
    {
        private readonly EcsFilterInject<Inc<VisibleComponent, VisibleListenerComponent>> _filter = default;

        private readonly EcsPoolInject<VisibleComponent> _visiblePool = default;
        private readonly EcsPoolInject<VisibleListenerComponent> _visibleListenerPool = default;

        public void Run(EcsSystems systems)
        {
            foreach (var entity in _filter.Value)
            {
                ref var visibleComponent = ref _visiblePool.Value.Get(entity);
                ref var visibleListenerComponent = ref _visibleListenerPool.Value.Get(entity);

                visibleListenerComponent.Listener.OnVisibleUpdate(visibleComponent.Value);
            }
        }
    }
}