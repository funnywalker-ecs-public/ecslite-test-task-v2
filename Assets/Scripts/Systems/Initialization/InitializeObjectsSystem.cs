using ECSLiteTestTask.Services;
using ECSLiteTestTask.Views;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace ECSLiteTestTask.Systems
{
    public class InitializeObjectsSystem : IEcsInitSystem
    {
        private readonly EcsCustomInject<IObjectFinderService> _objectFinderService = default;

        private readonly EcsWorldInject _ecsWorld = default;
        
        public void Init(EcsSystems systems)
        {
            var entityViews = _objectFinderService.Value.FindObjectsOfType<IEntityView>();
            foreach (var entityView in entityViews)
            {
                if (entityView.Entity < 0)
                {
                    var entity = _ecsWorld.Value.NewEntity();
                    entityView.Entity = entity;

                    var monoComponents = entityView.GetMonoComponents();
                    foreach (var monoComponent in monoComponents)
                    {
                        monoComponent.Register(entity, _ecsWorld.Value);
                    }
                }
            }
        }
    }
}
