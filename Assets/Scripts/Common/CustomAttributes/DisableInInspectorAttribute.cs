﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace ECSLiteTestTask.EditorUtils
{
    public class DisableInInspector : PropertyAttribute { }

    [CustomPropertyDrawer(typeof(DisableInInspector))]
    public class DisableInInspectorDrawer: PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginDisabledGroup(true);
            EditorGUI.PropertyField(position, property, label);
            EditorGUI.EndDisabledGroup();
        }
    }
}
#endif