namespace ECSLiteTestTask.Common
{
    public static class GameConstants
    {
        #region ECS

        public const string PHYSICS_WORLD = "physics";
        public const string EVENTS_WORLD = "events";

        #endregion

        #region Animations

        public const string ANIMATOR_BOOL_IDLE = "Idle";
        public const string ANIMATOR_BOOL_PRESS = "Press";
        public const string ANIMATOR_FLOAT_MOVESPEED = "MoveSpeed";

        #endregion
    }
}
