#if UNITY_EDITOR
using ECSLiteTestTask.EditorUtils;
#endif
using UnityEngine;

namespace ECSLiteTestTask.Configs
{
    public class Config : ScriptableObject
    {
#if UNITY_EDITOR
        [DisableInInspector]
#endif
        [SerializeField] private string _id;

        public string ID => _id;
    }
}
