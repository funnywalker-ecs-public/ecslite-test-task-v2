using UnityEngine;

namespace ECSLiteTestTask.Configs
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "ECSLiteTestTask/Configs/Game")]
    public class GameConfig : Config
    {
        [SerializeField] private PlayerConfig _playerConfig;
        [SerializeField] private TargetConfig _targetConfig;
        [Space(10)]
        [SerializeField] private float _doorOpenSpeed = 1.0f;
        [SerializeField] private float _doorOpenHeightThreshold = -5.0f;
        
        public PlayerConfig PlayerConfig => _playerConfig;
        public TargetConfig TargetConfig => _targetConfig;
        public float DoorOpenSpeed => _doorOpenSpeed;
        public float DoorOpenHeightThreshold => _doorOpenHeightThreshold;
    }
}
