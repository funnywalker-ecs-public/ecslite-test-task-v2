using UnityEngine;

namespace ECSLiteTestTask.Configs
{
    [CreateAssetMenu(fileName = "TargetConfig", menuName = "ECSLiteTestTask/Configs/Target")]
    public class TargetConfig : Config
    {
        [SerializeField] private GameObject _prefab;

        public GameObject Prefab => _prefab;
    }
}
